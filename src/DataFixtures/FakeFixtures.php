<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Fake;
use App\Entity\Moi;
use App\Entity\Competences;
use App\Entity\Projet;


class FakeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = \Faker\Factory::create('fr_FR');

        $fakeimg = ["img1.jpg" , "img2.jpg"];
        $moiimg = ["img1.jpg" , "img2.jpg"];
        $competencesimg = ["img1.jpg" , "img2.jpg"];
        $projetimg = ["img1.jpg" , "img2.jpg"];
//Entity fake
        for ($i = 0 ; $i < sizeof($fakeimg); $i++ ){
            $fake = new Fake();

            $fake->setTitle($faker->sentence($nbWords = 6, $variableNbWords = true));
            $fake->setDescription($faker->text($maxNbChars = 200));
            $fake->setImage($fakeimg[$i]);
            $fake->setUpdatedAt(new \DateTime('now'));

            $manager->persist($fake);

        }
//Entity moi
        for ($i = 0 ; $i < sizeof($fakeimg); $i++ ){
            $moi = new Moi();

            $moi->setTitle($faker->sentence($nbWords = 6, $variableNbWords = true));
            $moi->setDescription($faker->text($maxNbChars = 200));
            $moi->setImage($moiimg[$i]);
            $moi->setUpdatedAt(new \DateTime('now'));

            $manager->persist($moi);
        }
//Entity competences
        for ($i = 0 ; $i < sizeof($fakeimg); $i++ ){
            $competences = new Competences();

            $competences->setTitle($faker->sentence($nbWords = 6, $variableNbWords = true));
            $competences->setDescription($faker->text($maxNbChars = 200));
            $competences->setImage($competencesimg[$i]);
            $competences->setUpdatedAt(new \DateTime('now'));

            $manager->persist($competences);
        }
//Entity projet
        for ($i = 0 ; $i < sizeof($fakeimg); $i++ ){
            $projet = new Projet();

            $projet->setTitle($faker->sentence($nbWords = 6, $variableNbWords = true));
            $projet->setDescription($faker->text($maxNbChars = 200));
            $projet->setImage($projetimg[$i]);
            $projet->setUpdatedAt(new \DateTime('now'));

            $manager->persist($projet);
        }

        $manager->flush();
    }
}
