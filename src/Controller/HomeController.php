<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Fake;
use App\Entity\Moi;
use App\Entity\Competences;
use App\Entity\Projet;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function title(ObjectManager $manager)
    {
        $fake = $this->getDoctrine()->getRepository(Fake::class);
        $moi = $this->getDoctrine()->getRepository(Moi::class);
        $competences = $this->getDoctrine()->getRepository(Competences::class);
        $projet = $this->getDoctrine()->getRepository(Projet::class);

        return $this->render('home/index.html.twig', [
            'fakes' => $fake->findAll(),
            'mois' => $moi->findAll(),
            'competencess' => $competences->findAll(),
            'projets' => $projet->findAll(),
        ]);
    }
}
